﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    class World
    {
        public string[,] tab = new string[12,12];
        public int[,] sa = new int[12, 12];
        Random random = new Random();
        public void stworzPlansze()
        {
            int pom = 0;
            for (int i = 0; i < 12;i++ )
            {
                for(int j =0 ;j<12;j++)
                {
                    tab[i, j] = ".";
                }
            }
                for (int i = 1; i < 11; i++)
                {
                    for (int j = 1; j < 11; j++)
                    {
                        tab[i, j] = ".";
                        if (pom == 3)
                        {
                            tab[i, j] = "*";
                            pom = 0;
                        }
                        else
                        {
                            tab[i, j] = ".";
                        }
                        pom++;

                    }
                }
        }
        public void wyswietl()
        {
            for (int i = 1; i < 11; i++)
            {
                for (int j = 1; j < 11; j++)
                {
                    Console.Write(tab[i, j]);
                }
                Console.WriteLine();
            }
        }
        public void przetrwanie()
        {
            
            for (int i = 1; i < 11; i++)
            {
                for (int j = 1; j < 11; j++)
                {
                    int ileSasiadow=0;
 
                    if (tab[i -1 , j - 1] == "*")
                    {
                        ileSasiadow++;
                    }
                    if (tab[i - 1, j ] == "*")
                    {
                        ileSasiadow++;
                    }
                    if (tab[i - 1, j + 1] == "*")
                    {
                        ileSasiadow++;
                    }
                    if (tab[i , j - 1] == "*")
                    {
                        ileSasiadow++;
                    }
                    if (tab[i , j + 1] == "*")
                    {
                        ileSasiadow++;
                    }
                    if (tab[i + 1, j - 1] == "*")
                    {
                        ileSasiadow++;
                    }
                    if (tab[i + 1, j ] == "*")
                    {
                        ileSasiadow++;
                    }
                    if (tab[i + 1, j + 1] == "*")
                    {
                        ileSasiadow++;
                    }
                    sa[i,j]=ileSasiadow;
                    ileSasiadow=0;
                }
            }
        }

        public void wyswietl_sa()
        {
            for (int i = 1; i < 11; i++)
            {
                for (int j = 1; j < 11; j++)
                {
                    Console.Write(sa[i, j]);
                }
                Console.WriteLine();
            }
        }
        public void Update()
        {
            for (int i = 1; i < 11; i++)
            {
                for (int j = 1; j < 11; j++)
                {
                    if(sa[i,j]<2 || sa[i,j]>3)
                    {
                        tab[i, j] = ".";
                    }
                    if(tab[i,j]=="." && sa[i,j]==3)
                    {
                        tab[i, j] = "*";
                    }
                }
            }
        }
        public void Graj()
        {
            stworzPlansze();
            while(true)
            {
                wyswietl();
                przetrwanie();
                Update();
                Console.ReadLine();
            }
        }
    }
}
