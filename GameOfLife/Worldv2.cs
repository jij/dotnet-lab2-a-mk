﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    class Worldv2
    {
        int size;
        public Worldv2(int size)
        {
            this.size = size+2;
            tab = new string[this.size, this.size];
            sa = new int[this.size, this.size];
        }
        public string[,] tab;
        public int[,] sa;
        Random random = new Random();
        public void stworzPlansze()
        {
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    tab[i, j] = ".";
                }
            }
            for (int i = 1; i < size-1; i++)
            {
                for (int j = 1; j < size-1; j++)
                {
                    tab[i, j] = ".";
                    if (random.Next(2) == 0)
                    {
                        tab[i, j] = "*";
                    }
                    else
                    {
                        tab[i, j] = ".";
                    }

                }
            }
        }
        public void wyswietl()
        {
            for (int i = 1; i < size-1; i++)
            {
                for (int j = 1; j < size-1; j++)
                {
                    Console.Write(tab[i, j]);
                }
                Console.WriteLine();
            }
        }
        public void przetrwanie()
        {

            for (int i = 1; i < size-1; i++)
            {
                for (int j = 1; j < size-1; j++)
                {
                    int ileSasiadow = 0;

                    if (tab[i - 1, j - 1] == "*")
                    {
                        ileSasiadow++;
                    }
                    if (tab[i - 1, j] == "*")
                    {
                        ileSasiadow++;
                    }
                    if (tab[i - 1, j + 1] == "*")
                    {
                        ileSasiadow++;
                    }
                    if (tab[i, j - 1] == "*")
                    {
                        ileSasiadow++;
                    }
                    if (tab[i, j + 1] == "*")
                    {
                        ileSasiadow++;
                    }
                    if (tab[i + 1, j - 1] == "*")
                    {
                        ileSasiadow++;
                    }
                    if (tab[i + 1, j] == "*")
                    {
                        ileSasiadow++;
                    }
                    if (tab[i + 1, j + 1] == "*")
                    {
                        ileSasiadow++;
                    }
                    sa[i, j] = ileSasiadow;
                    ileSasiadow = 0;
                }
            }
        }

        public void wyswietl_sa()
        {
            for (int i = 1; i < size-1; i++)
            {
                for (int j = 1; j < size-1; j++)
                {
                    Console.Write(sa[i, j]);
                }
                Console.WriteLine();
            }
        }
        public void Update()
        {
            for (int i = 1; i < size-1; i++)
            {
                for (int j = 1; j < size-1; j++)
                {
                    if (sa[i, j] < 2 || sa[i, j] > 3)
                    {
                        tab[i, j] = ".";
                    }
                    if (tab[i, j] == "." && sa[i, j] == 3)
                    {
                        tab[i, j] = "*";
                    }
                }
            }
        }
        public void Graj()
        {
            stworzPlansze();
            while (true)
            {
                wyswietl();
                if (czyKoniec())
                {
                    Console.WriteLine("Koniec gry!");
                    Console.Read();
                    break;
                }
                przetrwanie();
                Update();
                Console.ReadLine();
            }
        }
        public bool czyKoniec()
        {
            for(int i =1;i<size-1;i++)
            {
                for(int j = 1;j<size;j++)
                {
                    if(tab[i,j]=="*")
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
