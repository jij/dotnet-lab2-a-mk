﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Worldv1 - 1 wersja gry,  bez losowej generacji planszy i bez spradzania czy zakonczono gre
//Worldv2 - 2 wersja gry, z losową generacją planszy i z sprawdzaniem czy zakonczono gre

namespace GameOfLife
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Podaj rozmiar kwadratowej planszy");
            int x;
            while (true)
            {
                string pom = Console.ReadLine();
                if (Int32.TryParse(pom, out x))
                {
                    Worldv2 w = new Worldv2(x);
                    w.Graj();
                    break;
                }
                else
                {
                    Console.WriteLine("Należy podać liczbę!");
                }
            }
         }
    }
}
